#pragma once

#include "Defs.h"

#include <cstdint>
#include <fstream>

#include "Containers.h"

/****************************************/ MCRYPTO_BEGIN /****************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES-128-ECB
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT EncryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key);
void        EncryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key,
                             ByteBufferT& outBuf);

ByteBufferT DecryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES-128-CBC
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT EncryptAES128CBC(const ByteBufferT& inBuf, const ByteBufferT& key,
                             const ByteBufferT& iv);

ByteBufferT DecryptAES128CBC(const ByteBufferT& inBuf, const ByteBufferT& key,
                             const ByteBufferT& iv);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES-128-CTR
///////////////////////////////////////////////////////////////////////////////////////////////////
class AES128CTREngine
{
public:
    AES128CTREngine(ByteBufferT&& key, uint64_t nonce);

    ByteBufferT operator()(const ByteBufferT& inBuf);

    void Reset();

private:
    ByteT nextByte();

    ByteBufferT key_;
    uint64_t    nonce_;
    uint64_t    counter_;

    ByteBufferT encryptBuf_;
    ByteBufferT block_;
    ByteT*      curBytePtr_;
};

/*****************************************/ MCRYPTO_END /*****************************************/