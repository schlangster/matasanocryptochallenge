#include "Ciphers.h"

#include <algorithm>

#include <openssl/evp.h>

#include "Util.h"

/****************************************/ PRIVATE_BEGIN /****************************************/

using namespace MCRYPTO;

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DoAES128ECB
///////////////////////////////////////////////////////////////////////////////////////////////////
void DoAES128ECB(const ByteT* in, size_t length, ByteT* out, ByteT* key, bool encrypt)
{
    EVP_CIPHER_CTX ctx;
    EVP_CIPHER_CTX_init(&ctx);

    if (EVP_CipherInit_ex(&ctx, EVP_aes_128_ecb(), NULL, key, NULL, encrypt? 1 : 0) > 0)
        EVP_Cipher(&ctx, out, in, length);

    EVP_CIPHER_CTX_cleanup(&ctx);
}

/*****************************************/ PRIVATE_END /*****************************************/

/****************************************/ MCRYPTO_BEGIN /****************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES-128-ECB
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT EncryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key)
{
    assert(key.Length() == 16);

    ByteBufferT outBuf( inBuf.Length() );
    DoAES128ECB(inBuf.begin(), inBuf.Length(), outBuf.begin(), key.begin(), true);
    return outBuf;
}

void EncryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key, ByteBufferT& outBuf)
{
    assert(key.Length() == 16);

    DoAES128ECB(inBuf.begin(), inBuf.Length(), outBuf.begin(), key.begin(), true);
}

ByteBufferT DecryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key)
{
    assert(key.Length() == 16);

    ByteBufferT outBuf( inBuf.Length() );
    DoAES128ECB(inBuf.begin(), inBuf.Length(), outBuf.begin(), key.begin(), false);
    return outBuf;
}

void DecryptAES128ECB(const ByteBufferT& inBuf, const ByteBufferT& key, ByteBufferT& outBuf)
{
    assert(key.Length() == 16);

    DoAES128ECB(inBuf.begin(), inBuf.Length(), outBuf.begin(), key.begin(), false);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// EncryptAES128CBC
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT EncryptAES128CBC(const ByteBufferT& inBuf, const ByteBufferT& key,
                             const ByteBufferT& iv)
{
    size_t blockSize = key.Length();
    size_t oddCount  = inBuf.Length() % blockSize;
    size_t fillCount = oddCount == 0 ? 0 : blockSize - oddCount;

    ByteBufferT outBuf( inBuf.Length() + fillCount );
    ByteBufferT blockBuf( blockSize );

    // Step 1: Initialization vector
    std::transform(
        iv.begin(), iv.end(),
        inBuf.begin(),
        blockBuf.begin(),
        [] (ByteT a, ByteT b) {
            return a ^ b;
        });

    DoAES128ECB(blockBuf.begin(), blockSize, outBuf.begin(), key.begin(), true);

    // Step 2: Encrypt the rest
    auto in     = inBuf.begin() + blockSize;
    auto out    = outBuf.begin();

    while (in != inBuf.end())
    {
        auto blockOut = blockBuf.begin();

        // Get blockCount or less bytes
        for (size_t i=0; i<blockSize && in != inBuf.end(); i++, in++, blockOut++)
            *blockOut = *in;

        // Xor block buffer and previous output (out has not been incremented yet)
        std::transform(
            blockBuf.begin(), blockBuf.end(),
            out,
            blockBuf.begin(),
            [] (ByteT a, ByteT b) {
                return a ^ b;
            });

        // Advance out to insert position
        out += blockSize;

        DoAES128ECB(blockBuf.begin(), blockSize, out, key.begin(), true);

        blockBuf.Clear();
    }

    return outBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DecryptAES128CBC
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT DecryptAES128CBC(const ByteBufferT& inBuf, const ByteBufferT& key,
                             const ByteBufferT& iv)
{
    size_t blockSize = key.Length();

    ByteBufferT outBuf( inBuf.Length() );

    auto in     = inBuf.begin();
    auto out    = outBuf.begin();

    DoAES128ECB(in, blockSize, out, key.begin(), false);

    std::transform(
        iv.begin(), iv.end(),
        out,
        out,
        [] (ByteT a, ByteT b) {
            return a ^ b;
        });

    in  += blockSize;
    out += blockSize;    

    while (in != inBuf.end())
    {
        DoAES128ECB(in, blockSize, out, key.begin(), false);        

        std::transform(
            out, out + blockSize,
            in - blockSize,
            out,
            [] (ByteT a, ByteT b) {
                return a ^ b;
            });

        in  += blockSize;
        out += blockSize;        
    }

    return outBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES128CTREngine
///////////////////////////////////////////////////////////////////////////////////////////////////
AES128CTREngine::AES128CTREngine(ByteBufferT&& key, uint64_t nonce) :
    key_( std::move(key) ),
    nonce_( nonce ),
    counter_( 0 ),
    encryptBuf_( 16 ),
    block_( 16 ),
    curBytePtr_( block_.end() )
{
}

ByteBufferT AES128CTREngine::operator()(const ByteBufferT& inBuf)
{
    size_t len = inBuf.Length();
    ByteBufferT outBuf( len );

    for (size_t i=0; i<len; i++)
        outBuf[i] = inBuf[i] ^ nextByte();

    return outBuf;
}

void AES128CTREngine::Reset()
{
    curBytePtr_ = block_.end();
    counter_ = 0;
}

ByteT AES128CTREngine::nextByte()
{
    // Generate next block?
    if (curBytePtr_ == block_.end())
    {
        curBytePtr_ = block_.begin();

        for (size_t i=0; i<8; i++)
            encryptBuf_[i] = (nonce_ << (i*8)) & 0xFF;

        for (size_t i=0; i<8; i++)
            encryptBuf_[8+i] = (counter_ << (i*8)) & 0xFF;

        EncryptAES128ECB(encryptBuf_, key_, block_);

        ++counter_;
    }

    auto result = *curBytePtr_;
    ++curBytePtr_;
    return result;
}

/*****************************************/ MCRYPTO_END /*****************************************/