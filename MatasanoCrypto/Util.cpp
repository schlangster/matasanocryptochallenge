#include "Util.h"

#include <algorithm>
#include <iostream>
#include <memory>
#include <utility>
#include <string>

#include "Algorithms.h"
#include "Ciphers.h"

/****************************************/ PRIVATE_BEGIN /****************************************/

using namespace MCRYPTO;

char base64_tbl[64] =
{
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
    't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
};

uint32_t undoShiftXor(uint32_t value, int shift, uint32_t mask = 0xFFFFFFFF)
{
    bool shouldUndoRightShift = shift < 0;
    if (shouldUndoRightShift)
        shift *= -1;

    uint32_t result = 0;

    for (size_t i=0; i*shift < 32; i++)
    {
        uint32_t partMask = shouldUndoRightShift ?
            (0xFFFFFFFF << (32 - shift)) >> (shift * i) :
            (0xFFFFFFFF >> (32 - shift)) << (shift * i);

        uint32_t part = value & partMask;
            
        value ^= shouldUndoRightShift ?
            (part >> shift) & mask :
            (part << shift) & mask;
        result |= part;
    }

    return result;
}

/*****************************************/ PRIVATE_END /*****************************************/

/****************************************/ MCRYPTO_BEGIN /****************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
/// EncodeBase64Character
///////////////////////////////////////////////////////////////////////////////////////////////////
char EncodeBase64Character(ByteT c)
{
    assert(c < 64);

    //// A - Z
    //if (c <= 25)
    //    return c + 65;
    //// a - z
    //else if (c <= 51)
    //    return c + 71;
    //// 0 - 9
    //else if (c <= 61)
    //    return c - 4;
    //// +
    //else if (c == 62)
    //    return '+';
    //else
    //    return '/';

    return base64_tbl[c];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DecodeBase64Character
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteT DecodeBase64Character(char c)
{
    //assert('+' <= c && c <= 'z');
    if (! ('+' <= c && c <= 'z'))
        printf("Invalid char: %d (%c)\n", (ByteT)c, c);

    if (c == '+')
        return 62;

    else if (c == '/')
        return 63;

    else if (c <= '9')
        return c + 4;

    else if (c <= 'Z')
        return c - 65;

    else
        return c - 71;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DecodeHexCharacter
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteT DecodeHexCharacter(char c)
{
    switch (c)
    {
    case '0':
        return 0;
    case '1':
        return 1;
    case '2':
        return 2;
    case '3':
        return 3;
    case '4':
        return 4;
    case '5':
        return 5;
    case '6':
        return 6;
    case '7':
        return 7;
    case '8':
        return 8;
    case '9':
        return 9;
    case 'A':
    case 'a':
        return 10;
    case 'B':
    case 'b':
        return 11;
    case 'C':
    case 'c':
        return 12;
    case 'D':
    case 'd':
        return 13;
    case 'E':
    case 'e':
        return 14;
    case 'F':
    case 'f':
        return 15;
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DecodeHexString
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT DecodeHexString(const char* inStr)
{
    size_t len = strlen(inStr) / 2;

    ByteBufferT buf( len );

    DecodeHexString(inStr, buf);

    return buf;
}

void DecodeHexString(const char* inStr, ByteBufferT& outBuf)
{
    bool flag = false;
    ByteT c = 0;

    auto out = outBuf.begin();

    while (*inStr != '\0')
    {
        ByteT t = DecodeHexCharacter(*inStr);

        if (!flag)
        {
            c |= t << 4;
        }
        else
        {
            c |= t;

            *out = c;
            c = 0;
            ++out;
        }   

        flag = !flag;
        ++inStr;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// EncodeBase64
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT EncodeBase64(const ByteBufferT& inBuf)
{
    size_t inLength = inBuf.Length();

    size_t oddCount  = inLength % 3;
    size_t fillCount = oddCount != 0 ? 3 - oddCount : 0;

    size_t outLength = 4 * ((inLength + fillCount) / 3);

    ByteBufferT outBuf( outLength );

    size_t          bytePos = 2;
    unsigned int    byteBuf = 0;

    auto out = outBuf.begin();
    auto in  = inBuf.begin();

    for (size_t i=0; i<inLength; i++, in++)
    {
        byteBuf |= (*in) << (bytePos*8);

        // Buffer filled with 3 bytes yet?
        if (bytePos == 0)
        {
            for (size_t j=0; j<4; j++)
            {
                *(out+(3-j)) = EncodeBase64Character(byteBuf & 63);
                byteBuf >>= 6;
            }

            out += 4;
            bytePos = 2;
            byteBuf = 0;
        }
        else
        {
            --bytePos;
        }
    }

    // Write remaining odd bytes and append fill byte markers
    if (fillCount != 0)
    {
        for (size_t j=0; j<4; j++)
        {
            *(out+(3-j)) = j < fillCount ? '=' : EncodeBase64Character(byteBuf & 63);
            byteBuf >>= 6;
        }
    }

    return outBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DecodeBase64
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT DecodeBase64(const ByteBufferT& inBuf)
{
    size_t length    = inBuf.Length();

    // Count fill byte markers at the end of the sequence
    size_t fillCount = 0;
    {
        auto tail = inBuf.end() - 1;
        if (*tail == '=')
            ++fillCount;
        --tail;
        if (*tail == '=')
            ++fillCount;
    }

    size_t outLength = 3 * (length / 4) - fillCount;

    // We don't have to read in the fill byte markers
    length -= fillCount;

    ByteBufferT outBuf( outLength );

    size_t          byteCount   = 3;
    unsigned int    byteBuf     = 0;

    auto out = outBuf.begin();
    auto in  = inBuf.begin();

    for (size_t i=0; i<length; i++, in++)
    {
        ByteT t = DecodeBase64Character(*in);

        byteBuf |= t << (byteCount*6);

        // Buffer filled with 4 characters yet?
        if (byteCount == 0)
        {
            for (size_t j=0; j<3; j++)
            {
                *(out+(2-j)) = byteBuf & 255;
                byteBuf >>= 8;
            }

            out += 3;
            byteCount = 3;
            byteBuf = 0;
        }
        else
        {
            --byteCount;
        }
    }

    if (fillCount != 0)
    {
        for (size_t j=0; j<3; j++)
        {
            if (j >= fillCount)
                *(out+(2-j)) = byteBuf & 255;
            byteBuf >>= 8;
        }
    }

    return outBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Concat
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT Concat(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2)
{
    ByteBufferT outBuf( inBuf1.Length() + inBuf2.Length() );

    std::copy(inBuf1.begin(), inBuf1.end(), outBuf.begin());
    std::copy(inBuf2.begin(), inBuf2.end(), outBuf.begin() + inBuf1.Length());

    return outBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Xor
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT Xor(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2)
{
    ByteBufferT outBuf( inBuf1.Length() );
    Xor(inBuf1, inBuf2, outBuf);
    return outBuf;
}

void Xor(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2, ByteBufferT& outBuf)
{
    TransformRange(inBuf1, inBuf2, outBuf, std::bit_xor<ByteT>());
}

ByteBufferT Xor(const ByteBufferT& inBuf, ByteT inChar)
{
    ByteBufferT outBuf( inBuf.Length() );
    Xor(inBuf, inChar, outBuf);
    return outBuf;
}

void Xor(const ByteBufferT& inBuf, ByteT inChar, ByteBufferT& outBuf)
{
    TransformRange(inBuf, outBuf,
        [&] (ByteT a){
            return a ^ inChar;
        });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// RepeatingXor
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT RepeatingXor(const ByteBufferT& inBuf, const ByteBufferT& keySeq)
{
    ByteBufferT outBuf( inBuf.Length() );
    RepeatingXor(inBuf, keySeq, outBuf);
    return outBuf;
}

void RepeatingXor(const ByteBufferT& inBuf, const ByteBufferT& keySeq, ByteBufferT& outBuf)
{
    auto key = keySeq.begin();

    TransformRange(inBuf, outBuf,
        [&] (ByteT a){
            auto result = a ^ *key;

            if (key == (keySeq.end() - 1))
                key = keySeq.begin();
            else
                ++key;

            return result;
        });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// CalculateCharacterScore
///////////////////////////////////////////////////////////////////////////////////////////////////
int CalculateCharacterScore(const ByteBufferT& inBuf)
{
    static const ByteT  printable_range_start  = 32;
    static const ByteT  printable_range_end    = 127;
    static const size_t printable_range_length = printable_range_end - printable_range_start;

    // The smaller the better
    int score = 0;

    using FreqT = std::pair<ByteT, unsigned int>;
    FreqT counts[printable_range_length];

    ByteT indexMap[printable_range_length];

    const char* testChars = "etaoinshrdlucmfwypvbgkqjxz";

    for (ByteT i=0; i<printable_range_length; i++)
        counts[i] = std::make_pair(printable_range_start + i, 0);

    // 48 -  57 : 0 - 9
    // 65 -  90 : A - Z
    // 97 - 122 : a - z

    auto cur = inBuf.begin();

    // Count frequencies
    for (size_t i=0; i<inBuf.Length(); i++, cur++)
    {
        ByteT c = *cur;

        if (c == '\t' || c == '\n' || c == '\r')
            continue;

        // Non printable? return worst score
        if(! (printable_range_start <= c && c < printable_range_end))
            return (std::numeric_limits<int>::max)();

        // To lower case
        if (65 <= c && c < 91)
            c += 32;

        // Adjust for array offset
        ByteT idx = c - printable_range_start;

        ++counts[idx].second;
    }

    // Sort desc by frequency
    std::sort(
        &counts[0],
        &counts[printable_range_length],
        [] (FreqT a, FreqT b) {
            return a.second > b.second;
        });

    // Build index map for fast lookup
    for (ByteT i=0; i<printable_range_length; i++)
    {
        indexMap[counts[i].first - printable_range_start] = i; 
    }

    int testPos = 0;

    for (const char* p = testChars; *p != '\0'; p++, testPos++)
    {
        ByteT c = *p;
        ByteT idx = c - printable_range_start;

        int actualPos = indexMap[idx];
           
        score += std::abs(testPos - actualPos);
    }

    return score;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// FindXorKey
///////////////////////////////////////////////////////////////////////////////////////////////////
XorKeyResult FindXorKey(const ByteBufferT& inBuf)
{
    ByteBufferT xorBuf( inBuf.Length() );

    return FindXorKey(inBuf, xorBuf);
}

XorKeyResult FindXorKey(const ByteBufferT& inBuf, ByteBufferT& xorBuf)
{
    int     minScore    = (std::numeric_limits<int>::max)();
    ByteT   key         = 0;

    // Find key with minimal score
    for (int i=0; i<255; i++)
    {
        Xor(inBuf, (ByteT)i, xorBuf);

        auto score = CalculateCharacterScore(xorBuf);

        if (score < minScore)
        {
            minScore = score;
            key  = i;
        }
    }

    Xor(inBuf, key, xorBuf);
    auto x = CalculateCharacterScore(xorBuf);

    return XorKeyResult{ key, minScore };
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// CalculateHammingDistance
///////////////////////////////////////////////////////////////////////////////////////////////////
int CalculateHammingDistance(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2)
{
    assert(inBuf1.Length() == inBuf2.Length());

    int result = 0;
    auto in1 = inBuf1.begin();
    auto in2 = inBuf2.begin();

    for (size_t i=0; i<inBuf1.Length(); i++, in1++, in2++)
    {
        ByteT t = *in1 ^ *in2;

        for (size_t j=0; j<8; j++)
        {
            if (t & 1)
                ++result;
            t >>= 1;
        }
    }

    return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// DetectRepeatingBlocks
///////////////////////////////////////////////////////////////////////////////////////////////////
RepeatingBlocksResult DetectRepeatingBlocks(const ByteBufferT& inBuf, size_t blockSize,
                                            bool aligned, bool successive)
{
    // Non-successive must be aligned
    assert(successive || aligned);

    size_t blockCount = inBuf.Length() / blockSize;
    
    size_t n = aligned ?
        blockCount - 1:
        (blockCount - 1) * blockSize;

    size_t k = aligned ? blockSize : 1;

    auto block1Begin = inBuf.begin();
    auto block1End   = block1Begin + blockSize;
    

    for (size_t i=0; i<n; i++, block1Begin += k, block1End += k)
    {
        auto block2Begin = block1End;

        for (size_t j=i+1; j < (successive ? i+2 : n); j++, block2Begin += k)
            if (std::equal(block1Begin, block1End, block2Begin))
                return RepeatingBlocksResult{ true, aligned ? i * blockSize : i};
    }

    return RepeatingBlocksResult{ false, 0};
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AddPKCS7Padding
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT AddPKCS7Padding(const ByteBufferT& inBuf, size_t blockSize)
{
    size_t inLen = inBuf.Length();

    size_t oddCount = inLen % blockSize;
    size_t padCount = blockSize - oddCount;
    
    assert(padCount < 256);
    ByteT padByte = padCount;

    ByteBufferT padded( inLen + padCount );

    std::copy(inBuf.begin(), inBuf.end(), padded.begin());

    auto out = padded.end() - padCount;
    for (size_t i=0; i<padCount; i++, out++)
        *out = padByte;

    return padded;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// RemovePKCS7Padding
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT RemovePKCS7Padding(const ByteBufferT& inBuf)
{
    auto last = inBuf.end() - 1;
    ByteT padLen = *last;

    assert(0 < padLen && padLen <=16);
    assert(inBuf.Length() >= 16);

    size_t outLen = inBuf.Length() - padLen;

    ByteBufferT outBuf( outLen );

    std::copy(inBuf.begin(), inBuf.begin() + outLen, outBuf.begin());

    return outBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ValidatePKCS7Padding
///////////////////////////////////////////////////////////////////////////////////////////////////
bool ValidatePKCS7Padding(const ByteBufferT& inBuf)
{
    auto last = inBuf.end() - 1;
    ByteT padLen = *last;

    if (padLen == 0)
        return false;

    if (padLen > inBuf.Length())
        return false;

    return std::all_of(inBuf.end() - padLen, last,
        [&] (int v) { return v == padLen; });
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// GetRandomGeneratorInstance
///////////////////////////////////////////////////////////////////////////////////////////////////
std::mt19937& GetRandomGeneratorInstance()
{
    struct Generator_
    {
        Generator_() :
            rd( ),
            gen( rd() )
        {}

        std::random_device  rd;
        std::mt19937        gen;
    };

    static Generator_ instance;

    return instance.gen;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// GenerateRandomBytes
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT GenerateRandomBytes(size_t length)
{
    ByteBufferT outBuf( length );
    GenerateRandomBytes(outBuf);
    return outBuf;
}

void GenerateRandomBytes(ByteBufferT& outBuf)
{
    auto& gen = GetRandomGeneratorInstance();
    std::uniform_int_distribution<int> dist( 0, 255 );

    std::generate(
        outBuf.begin(),
        outBuf.end(),
        [&] () { return dist(gen); });
}

void GenerateRandomBytes(ByteT* start, ByteT* end)
{
    auto& gen = GetRandomGeneratorInstance();
    std::uniform_int_distribution<int> dist( 0, 255 );

    std::generate(
        start,
        end,
        [&] () { return dist(gen); });
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES128EncryptionOracle
///////////////////////////////////////////////////////////////////////////////////////////////////
AES128EncryptionOracle::AES128EncryptionOracle() :
    Mode( mode_random ),
    key_( GenerateRandomBytes(16) ),
    iv_( GenerateRandomBytes(16) ),
    coinFlipDist_( 0, 1 ),
    shouldUseECB_( false )
{
}

void AES128EncryptionOracle::GenerateNewKey()
{
    key_ = GenerateRandomBytes(16);
}

void AES128EncryptionOracle::GenerateNewIV()
{
    iv_ = GenerateRandomBytes(16);
}

void AES128EncryptionOracle::GeneratePrefix(size_t min, size_t max)
{
    if (max == 0)
    {
        prefix_ = ByteBufferT( min );
    }
    else
    {
        assert(min < max);

        auto& gen = GetRandomGeneratorInstance();
        std::uniform_int_distribution<size_t> paddingDist( min, max );
        prefix_ = GenerateRandomBytes(paddingDist(gen));
    }
}

void AES128EncryptionOracle::GenerateSuffix(size_t min, size_t max)
{
    if (max == 0)
    {
        suffix_ = ByteBufferT( min );
    }
    else
    {
        assert(min < max);

        auto& gen = GetRandomGeneratorInstance();
        std::uniform_int_distribution<size_t> paddingDist( min, max );
        suffix_ = GenerateRandomBytes(paddingDist(gen));
    }
}

ByteBufferT AES128EncryptionOracle::Encrypt(const ByteBufferT& inBuf)
{
    auto& gen = GetRandomGeneratorInstance();

    shouldUseECB_ =
        Mode == mode_ecb ||
        (Mode == mode_random && coinFlipDist_(gen) == 0);

    size_t prefixLen = prefix_.Length();
    size_t suffixLen = suffix_.Length();

    if (prefixLen + suffixLen > 0)
    {
        ByteBufferT buf( inBuf.Length() + prefixLen + suffixLen);

        std::copy(prefix_.begin(), prefix_.end(), buf.begin());
        std::copy(inBuf.begin(), inBuf.end(), buf.begin() + prefixLen);
        std::copy(suffix_.begin(), suffix_.end(), buf.end() - suffixLen);

        auto paddedBuf = AddPKCS7Padding(buf, 16);

        return shouldUseECB_ ? 
            EncryptAES128ECB(paddedBuf, key_) :
            EncryptAES128CBC(paddedBuf, key_, iv_);
    }
    else
    {
        return shouldUseECB_ ? 
            EncryptAES128ECB(inBuf, key_) :
            EncryptAES128CBC(inBuf, key_, iv_);
    }   
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// FindKeySize
///////////////////////////////////////////////////////////////////////////////////////////////////
size_t FindKeySize(IEncryptingOracle& oracle, size_t maxSize)
{
    size_t keySize = 0;    

    ByteBufferT testBuf( maxSize );
    {
        auto p = testBuf.begin();
        for (size_t i=0; i<maxSize; i++, p++)
            *p = 'A';
    }

    ByteBufferT previous;
    ByteBufferT current   = oracle.Encrypt(testBuf);

    auto editPos = testBuf.begin() + 1;

    for (size_t i=1; i<maxSize; i++, editPos++)
    {
        *editPos = 'x';
            
        previous = std::move(current);
        current  = oracle.Encrypt(testBuf);

        if (std::equal(current.begin(), current.begin() + i, previous.begin()))
        {
            keySize = i;
            break;
        }
                
    }

    return keySize;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// FindKeySize
///////////////////////////////////////////////////////////////////////////////////////////////////
size_t FindKeySize2(IEncryptingOracle& oracle, size_t maxSize)
{
    size_t keySize = 0;

    for (size_t k=2; k<maxSize; k++)
    {
        size_t len = k * 3 - 1;

        ByteBufferT testBuf( len );

        auto p = testBuf.begin();
        for (size_t i=0; i<len; i++, p++)
            *p = 'A';
            
        auto encrypted = oracle.Encrypt(testBuf);

        if (DetectRepeatingBlocks(encrypted, k, false, true).Found)
        {
            keySize = k;
            break;
        }                
    }

    return keySize;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// FindVulnerableBlock
///////////////////////////////////////////////////////////////////////////////////////////////////
struct VulnerableBlockResult
{
    size_t PaddingLength;
    size_t Offset;
};

VulnerableBlockResult FindVulnerableBlock(IEncryptingOracle& oracle, size_t blockSize)
{
    size_t padding = 0;
    size_t index   = 0;

    size_t c = 0;

    // Increment input length until a repeating block is found
    while (true)
    {
        ByteBufferT testBuf( c );
        {
            auto p = testBuf.begin();
            for (size_t i=0; i<c; i++, p++)
                *p = 'A';
        }

        auto encryted = oracle.Encrypt(testBuf);

        auto repeatInfo = DetectRepeatingBlocks(encryted, blockSize, true, true);

        if (repeatInfo.Found)
        {
            // Blocks must be next to each other, otherwise something is not right
            // Must've fed at data for 2 blocks
            assert(c >= (2 * blockSize));

            size_t padding = c - (2 * blockSize);
            

            return VulnerableBlockResult{ padding, repeatInfo.Offset };
        }

        ++c;
        assert(c < 1024); // 1KB limit
    }  

    return VulnerableBlockResult{ 0, 0 };
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ExecuteECB1ByteAttack
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT ExecuteECB1ByteAttack(IEncryptingOracle& oracle, size_t blockSize)
{
    // Oracle might add prefix bytes, so first search for a vulnerable block to launch the attack.
    // Vulnerable means that our input completely fills the target block (and successive blocks).
    auto targetBlock = FindVulnerableBlock(oracle, blockSize);

    size_t paddingLen   = targetBlock.PaddingLength;
    size_t targetOffset = targetBlock.Offset;

    // No other input besides appended mystery string
    size_t mysteryLen = oracle.Encrypt(ByteBufferT( paddingLen )).Length() - targetOffset;

    // -- Setup dictionary
    using DictionaryT = std::unordered_map<ByteT,ByteBufferT>;

    DictionaryT dict;

    std::vector<ByteT> testChars{ '\n', '\t', '\r' };

    // Add printable chars
    for (ByteT c = 32; c<=126; c++)
        testChars.push_back(c);

    // -- Setup test buffer

    // Mystery must fit inside the test buffer, but the test buffer len must
    // also be a multiple of keySize.

    size_t alignedMysteryLen =  ((mysteryLen + (blockSize-1)) / blockSize) * blockSize;
    size_t testLen = paddingLen + alignedMysteryLen;

    ByteBufferT testBuf( testLen );

    // Fill with dummy characters
    for (auto& e : testBuf)
        e = '=';

    // -- Find key one byte at a time
    auto testPos = testBuf.end() - 1;

    for (size_t i=0; i<mysteryLen; i++)
    {
        for (const auto& c : testChars)
        {
            *testPos = c;
            auto encrypted = oracle.Encrypt(testBuf);
            encrypted.Truncate(encrypted.Length() - alignedMysteryLen - targetOffset);

            dict[c] = std::move(encrypted);
        }

        size_t prefixLen = testLen - i - 1;

        ByteBufferT encryptBuf( prefixLen );

        std::copy(
            testBuf.begin(),
            testBuf.begin() + prefixLen,
            encryptBuf.begin());

        auto encrypted = oracle.Encrypt(encryptBuf);
        encrypted.Truncate(encrypted.Length() - alignedMysteryLen - targetOffset);

        for (const auto& kv : dict)
        {
            const auto& t = kv.second;

            if (std::equal(t.begin() + targetOffset, t.end(), encrypted.begin() + targetOffset))
            {
                *testPos = kv.first;
                break;
            }
        }

        // Rotate left by 1
        if (i != mysteryLen - 1)
            std::rotate(testBuf.begin(), testBuf.begin() + 1, testBuf.end());
    }

    // Remove potential padding bytes
    std::rotate(testBuf.begin(), testBuf.begin() + (testLen - mysteryLen), testBuf.end());
    testBuf.Truncate(testLen - mysteryLen);

    return testBuf;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ParseCookieString
///////////////////////////////////////////////////////////////////////////////////////////////////
CookieT ParseCookieString(const char* str, char separator)
{
    enum State_
    {
        scan_key,
        scan_value
    };

    CookieT cookie;

    State_ state = scan_key;

    auto cur        = str;
    auto keyStart   = str;
    auto valueStart = str;

    

    //for (auto cur = str; *cur != '\0'; cur++)
    while (true)
    {
        auto c = *cur;

        // scan_key
        if (state == scan_key)
        {
            if (c == '=')
            {
                valueStart = cur + 1;

                state = scan_value;
            }
            else if (c == '\0')
            {
                assert(false);
            }
        }
        // scan_value
        else if (state == scan_value)
        {
            if (c == separator || c == '\0')
            {
                cookie.emplace(
                    std::string( keyStart, valueStart - 1  ),
                    std::string( valueStart, cur ));

                // More K/V pairs?
                if (c == separator)
                {
                    keyStart = cur + 1;
                    state = scan_key;
                }
                // Done?
                else if (c == '\0')
                {
                    break;
                }
            }
            else if (c == '=')
            {
                assert(false);
            }
        }

        ++cur;
    }

    return cookie;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// EncodeCookie
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT EncodeCookie(const CookieT& cookie)
{
    bool isFirst = true;

    std::string result;

    for (const auto& kv : cookie)
    {
        if (!isFirst)
            result.append("&");
        else
            isFirst = false;

        result.append(kv.first.c_str());
        result.append("=");
        result.append(kv.second.c_str());
    }

    return ByteBufferT( result.c_str() );
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// GenerateProfileCookie
///////////////////////////////////////////////////////////////////////////////////////////////////
CookieT GenerateProfileCookie(const char* email)
{
    CookieT cookie;

    std::string emailStr(email);

    for (auto& c : emailStr)
        if (c == '=' || c == '&')
            c = '_';

    cookie.emplace("email", emailStr.c_str());
    cookie.emplace("uid", "10");
    cookie.emplace("role", "user");

    return cookie;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// MT19937 - adapted code from http://de.wikipedia.org/wiki/Mersenne-Twister
///////////////////////////////////////////////////////////////////////////////////////////////////
MT19937::MT19937(uint32_t seed) :
    index_( n )
{
    initialize(seed);
}

MT19937::MT19937(uint32_t* state) :
    index_( n )
{
    for (size_t i=0; i<n; i++, state++)
        y_[i] = *state;
}

uint32_t MT19937::operator()()
{
    if (index_ == n)
    {
        generateNext();
        index_ = 0;
    }

    uint32_t r = y_[index_++];
    
    r ^= (r >> 11);
    r ^= (r <<  7) & 0x9d2c5680;
    r ^= (r << 15) & 0xefc60000;
    r ^= (r >> 18);

    return r;
}

void MT19937::initialize(uint32_t seed)
{
    y_[0] = seed;
 
    for (size_t i=1; i<n; i++)
        y_[i] = (1812433253UL * (y_[i-1] ^ (y_[i-1] >> 30)) + i);
}

void MT19937::generateNext()
{
    static const uint32_t a[2] = { 0, 0x9908b0df };

    uint32_t h;
    size_t i = 0;
 
    for (; i<n-m; i++)
    {
        h = (y_[i] & 0x80000000) | (y_[i+1] & 0x7fffffff);
        y_[i] = y_[i+m] ^ (h >> 1) ^ a[h & 1];
    }

    for (; i<n-1; i++)
    {
        h = (y_[i] & 0x80000000) | (y_[i+1] & 0x7fffffff);
        y_[i] = y_[i+(m-n)] ^ (h >> 1) ^ a[h & 1];
    }
 
    h = (y_[n-1] & 0x80000000) | (y_[0] & 0x7fffffff);
    y_[n-1] = y_[m-1] ^ (h >> 1) ^ a[h & 1];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// MT19937Untemper
///////////////////////////////////////////////////////////////////////////////////////////////////
uint32_t MT19937Untemper(uint32_t r)
{
    r = undoShiftXor(r, -18);
    r = undoShiftXor(r, 15, 0xefc60000);
    r = undoShiftXor(r, 7, 0x9d2c5680);
    r = undoShiftXor(r, -11);

    return r;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/*****************************************/ MCRYPTO_END /*****************************************/