#pragma once

#include "Defs.h"

#include <algorithm>
#include <functional>

#include "Containers.h"
#include "Util.h"

/****************************************/ MCRYPTO_BEGIN /****************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
/// TransformRange
///////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T, typename TOut, typename F>
void TransformRange(const T& inRange, TOut& outRange, F& func)
{
    using ValueT = typename GetValueType<T>::Type;

    assert(GetRangeLength(inRange) <= GetRangeLength(outRange));

    std::transform(
        std::begin(inRange), std::end(inRange),
        std::begin(outRange),
        func);
}

template <typename T1, typename T2, typename TOut, typename F>
void TransformRange(const T1& inRange1, const T2& inRange2, TOut& outRange, F& func)
{
    using ValueT = typename GetValueType<T1>::Type;

    assert(GetRangeLength(inRange1) <= GetRangeLength(inRange2));
    assert(GetRangeLength(inRange1) <= GetRangeLength(outRange));

    std::transform(
        std::begin(inRange1), std::end(inRange1),
        std::begin(inRange2),
        std::begin(outRange),
        func);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Transpose
///////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T, typename F>
void Transpose(const T& range, size_t blockCount, F& func)
{
    using ValueT = typename GetValueType<T>::Type;

    auto first  = std::begin(range);
    auto last   = std::end(range);

    size_t inLen    = std::distance(first, last);
    size_t oddCount = inLen % blockCount;
    size_t divider  = oddCount == 0 ? blockCount : oddCount;
    size_t blockLen = (inLen + (blockCount-1)) / blockCount;

    FixedBuffer<ValueT> blockBuf( blockLen );

    for (size_t i=0; i<blockCount; i++)
    {
        if (i == divider)
        {
            --blockLen;
            blockBuf.Truncate(1);
        }

        auto in  = first + i;
        auto out = blockBuf.begin();
        for (size_t j=0; j<blockLen; j++, out++, in+=blockCount)
            *out = *in;

        func(blockBuf);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ReplaceChar
///////////////////////////////////////////////////////////////////////////////////////////////////
template <typename TResult, typename TIn, typename TDict>
TResult ReplaceChar(const TIn& inputRange, const TDict& dictRange)
{
    using ValueT        = typename GetValueType<TIn>::Type;
    using DictPairT     = typename GetValueType<TDict>::Type;
    using MappedTypeT   = typename DictPairT::second_type;

    // Map bytes to their replacements, or null if not replaced
    const MappedTypeT* byteMap[256] = { nullptr };

    size_t maxLen = 1;

    for (const auto& kv : dictRange)
    {
        byteMap[kv.first] = &kv.second;

        size_t t = GetRangeLength(kv.second);
        if (maxLen < t)
            maxLen = t;
    }

    // Allocate temporary buffer with worst-case length
    FixedBuffer<ValueT> tempBuffer( maxLen * GetRangeLength(inputRange) );

    auto tempOut = tempBuffer.begin();

    for (ByteT b : inputRange)
    {
        auto t = byteMap[b];

        if (t == nullptr)
        {
            *tempOut = b;
            ++tempOut;
        }
        else
        {
            for (const auto& v : *t)
            {
                *tempOut = v;
                ++tempOut;
            }
        }
    }

    // Create result by copying used range of temp buffer
    return TResult( tempBuffer.begin(), tempOut );
}

/*****************************************/ MCRYPTO_END /*****************************************/