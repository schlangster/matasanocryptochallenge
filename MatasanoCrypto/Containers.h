#pragma once

#include "Defs.h"

#include <fstream>
#include <memory>
#include <string>

/****************************************/ MCRYPTO_BEGIN /****************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
/// FixedBuffer
///////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
class FixedBuffer
{
public:
    using value_type = T;

    // Default ctor
    FixedBuffer() :
        length_( 0 ),
        data_( nullptr )
    {}

    // Deleted copy ctor
    FixedBuffer(const FixedBuffer&) = delete;

    // Move ctor
    FixedBuffer(FixedBuffer&& other) :
        length_( other.length_),
        data_( std::move(other.data_) )
    {
        other.length_ = 0;
    }

    // Construct with given buffer length
    explicit FixedBuffer(size_t length) :
        length_( length ),
        data_( length > 0 ? new T[length] : nullptr )
    {}

    // Construct with data from null-terminated string (allows implicit conversion)
    FixedBuffer(const char* strData) :
        length_( strlen(strData) ),
        data_( new T[length_] )
    {
        auto out = begin();

        while (*strData != '\0')
        {
            *out = *strData;
            ++out; ++strData;
        }
    }

    // Construct from pair of iterators
    template <typename TInput>
    FixedBuffer(const TInput& first, const TInput& last) :
        length_( std::distance(first,last) ),
        data_( new T[length_] )
    {
        std::copy(first, last, begin());
    }

    // Deleted copy assignment
    FixedBuffer& operator=(const FixedBuffer&) = delete;

    // Move assignment
    FixedBuffer& operator=(FixedBuffer&& other)
    {
        length_ = other.length_;
        data_ = std::move(other.data_);

        return *this;
    }

    // Reallocate buffer with given length
    void Reset(size_t length)
    {
        length_ = length;
        data_.reset(length > 0 ? new T[length] : nullptr);
    }

    size_t  Length() const  { return length_; }

    // STL iterator compatible
    T*  begin() const   { return &data_[0]; }
    T*  end() const     { return &data_[length_]; }

    // Ignore trailing bytes without requiring reallocation
    void Truncate(size_t count)
    {
        assert(length_ >= count);

        length_ -= count;
    }

    // Ignore trailing bytes without requiring reallocation
    void TruncateTo(size_t newLength)
    {
        assert(newLength <= length_);
        Truncate(length_ - newLength);
    }

    void Clear(T val = 0)
    {
        for (auto& e : *this)
            e = val;
    }

    // Copy the contents of this buffer
    FixedBuffer Copy() const
    {
        FixedBuffer t( Length() );
        std::copy(begin(), end(), t.begin());
        return t;
    }

    T& operator[](size_t index)
    {
        return data_[index];
    }

    const T& operator[](size_t index) const
    {
        return data_[index];
    }

    // Compare buffer contents
    bool operator==(const FixedBuffer& other) const
    {
        if (Length() != other.Length())
            return false;

        return std::equal(begin(), end(), other.begin());
    }

    bool operator!=(const FixedBuffer& other) const
    {
        return !(*this == other);
    }

    void PrintBytes() const
    {
        auto p = begin();
        for (size_t i=0; i<Length(); i++, p++)
            printf("%02x", *p);
        printf("\n");
    }

    void PrintString() const
    {
        printf("%.*s\n", Length(), begin());
    }

    static FixedBuffer ReadFromFile(const char* fileName, bool skipNewlines = true)
    {
        std::ifstream inFile(fileName, std::ios::binary);
        assert(inFile.is_open());

        inFile.seekg(0, inFile.end);
        size_t len = static_cast<size_t>(inFile.tellg());

        FixedBuffer outBuf( len );
        inFile.seekg(0, inFile.beg);

        auto out = outBuf.begin();

        size_t skipCount = 0;

        char c;
        while(inFile.get(c))
        {
            if (!skipNewlines || (c != '\n' && c != '\r'))
            {
                *out = c;
                ++out;
            }
            else
            {
                ++skipCount;
            }
        }


        outBuf.Truncate(skipCount);

        return outBuf;
    }

private:
    size_t                  length_;
    std::unique_ptr<T[]>    data_;
};

using ByteBufferT = FixedBuffer<ByteT>;

/*****************************************/ MCRYPTO_END /*****************************************/