
#include <chrono>
#include <ctime>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <queue>
#include <unordered_map>
#include <thread>

#include "Algorithms.h"
#include "Ciphers.h"
#include "Containers.h"
#include "Util.h"

using MCRYPTO::ByteT;
using MCRYPTO::ByteBufferT;

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set 1: Basics
///////////////////////////////////////////////////////////////////////////////////////////////////
namespace set1
{
    void challenge1()
    {
        using MCRYPTO::DecodeHexString;
        using MCRYPTO::EncodeBase64;
        using MCRYPTO::DecodeBase64;

        printf("\n===== Challenge 1 =====\n\n");

        const char* inputStr = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";

        auto buf = DecodeHexString(inputStr);

        buf.PrintString();
    
        auto encoded = EncodeBase64(buf);

        encoded.PrintString();

        auto decoded = DecodeBase64(encoded);

        decoded.PrintString();
    }

    void challenge2()
    {
        using MCRYPTO::DecodeHexString;
        using MCRYPTO::Xor;

        printf("\n===== Challenge 2 =====\n\n");

        const char* inputStr1 = "1c0111001f010100061a024b53535009181c";
        const char* inputStr2 = "686974207468652062756c6c277320657965";

        size_t inputLength = strlen(inputStr1) / 2;

        auto inputBuf1 = DecodeHexString(inputStr1);
        auto inputBuf2 = DecodeHexString(inputStr2);

        ByteBufferT xorBuf( inputLength );
        
        Xor(inputBuf1, inputBuf2, xorBuf);

        printf("%s\n", inputStr1);
        printf("%s\n", inputStr2);

        xorBuf.PrintString();
    }

    void challenge3()
    {
        using MCRYPTO::DecodeHexString;
        using MCRYPTO::Xor;
        using MCRYPTO::FindXorKey;

        printf("\n===== Challenge 3 =====\n\n");

        const char* inputStr = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";

        size_t inputLength = strlen(inputStr) / 2;

        ByteBufferT inBuf( inputLength );
        ByteBufferT xorBuf( inputLength );

        DecodeHexString(inputStr, inBuf);

        auto result = FindXorKey(inBuf);

        Xor(inBuf, result.Key, xorBuf);

        printf("Min score: %d\n", result.Score);
        xorBuf.PrintString();
    }

    void challenge4()
    {
        using MCRYPTO::DecodeHexString;
        using MCRYPTO::Xor;
        using MCRYPTO::FindXorKey;

        printf("\n===== Challenge 4 =====\n\n");

        std::ifstream inFile("Data/4.txt");

        assert(inFile.is_open());

        std::string line;

        ByteBufferT decodeBuffer;
        ByteBufferT xorBuffer;

        int   minScore  = (std::numeric_limits<int>::max)();

        int lineNumber = 1;

        while (std::getline(inFile, line))
        {
            size_t len = line.length() / 2;

            if (decodeBuffer.Length() != len)
            {
                decodeBuffer.Reset(len);
                xorBuffer.Reset(len);
            }

            DecodeHexString(line.data(), decodeBuffer);
        
            auto result = FindXorKey(decodeBuffer, xorBuffer);

            int t = 0;

            if (result.Score < minScore)
            {
                minScore = result.Score;

                printf("line=%d score=%d key=%d\t", lineNumber, result.Score, result.Key);

                FindXorKey(decodeBuffer, xorBuffer);
                Xor(decodeBuffer, result.Key, xorBuffer);

                xorBuffer.PrintString();
                printf("\n");
            }   

            ++lineNumber;
        }
    }

    void challenge5()
    {
        using MCRYPTO::RepeatingXor;

        printf("\n===== Challenge 5 =====\n\n");

        std::string str;
        str += "Burning 'em, if you ain't quick and nimble\n";
        str += "I go crazy when I hear a cymbal";

        auto encrypted = RepeatingXor(str.c_str(), "ICE");

        encrypted.PrintBytes();
    }

    void challenge6()
    {
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::FindXorKey;
        using MCRYPTO::CalculateHammingDistance;
        using MCRYPTO::RepeatingXor;
        using MCRYPTO::CalculateCharacterScore;
        using MCRYPTO::Transpose;

        printf("\n===== Challenge 6 =====\n\n");

        auto inputBuf = ByteBufferT::ReadFromFile("Data/6.txt");
        auto decoded  = DecodeBase64(inputBuf);

        size_t keySize = 40;

        ByteBufferT blocks[4] =
        {
            ByteBufferT( keySize ),
            ByteBufferT( keySize ),
            ByteBufferT( keySize ),
            ByteBufferT( keySize )
        };

        using ScoreT = std::pair<double,size_t>;
        std::priority_queue<ScoreT> minScores;

        for (size_t i=keySize; i>=2; i--)
        {
            auto cur = decoded.begin();

            for (size_t j=0; j<4; j++, cur += keySize)
                std::copy(cur, cur+keySize, blocks[j].begin());

            size_t hammingSum   = 0;
            size_t hammingCount = 0;    

            for (size_t j=0; j<4; j++)
                for (size_t k=j+1; k<4; k++, hammingCount++)
                    hammingSum += CalculateHammingDistance(blocks[j], blocks[k]);

            double score = hammingSum;

            //printf("Sum %d, count %d\n", hammingSum, hammingCount);
            score /= hammingCount != 0 ? hammingCount : 1.0;
            score /= i;

            //printf("Keysize: %d, Score: %f\n", i, score);

            minScores.emplace(score, i);
            if (minScores.size() > 10)
                minScores.pop();

            blocks[0].Truncate(1);
            blocks[1].Truncate(1);
            blocks[2].Truncate(1);
            blocks[3].Truncate(1);
        }

        // Try decrypting with N best keysizes
        while (!minScores.empty())
        {
            auto score = minScores.top();
            minScores.pop();

            size_t keySize = score.second;

            ByteBufferT keyBuf( keySize );
            auto keyOut = keyBuf.begin();

            Transpose(decoded, keySize,
                [&] (const ByteBufferT& block)
                {
                    *keyOut = FindXorKey(block).Key;
                    ++keyOut;
                });

            auto decrypted = RepeatingXor(decoded, keyBuf);

            if (CalculateCharacterScore(decrypted) < (std::numeric_limits<int>::max)())
            {
                printf("Found potential match.\n");
                printf("KeySize: %d, Score: %f\n", keySize, score.first);
                keyBuf.PrintBytes();
                decrypted.PrintString();
            }
        }
    }

    void challenge7()
    {
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::DecryptAES128ECB;

        printf("\n===== Challenge 7 =====\n\n");

        auto inputBuf = ByteBufferT::ReadFromFile("Data/7.txt");

        auto decoded = DecodeBase64(inputBuf);

        auto decrypted = DecryptAES128ECB(decoded, "YELLOW SUBMARINE");

        decrypted.PrintString();
    }

    void challenge8()
    {
        using MCRYPTO::DecodeHexString;
        using MCRYPTO::DetectRepeatingBlocks;

        printf("\n===== Challenge 8 =====\n\n");

        std::ifstream inFile("Data/8.txt");

        assert(inFile.is_open());

        std::string line;

        int lineNumber = 1;

        while (std::getline(inFile, line))
        {
            ByteBufferT buf = DecodeHexString(line.c_str());

            if (DetectRepeatingBlocks(buf, 16, true, false).Found)
            {
                printf("Detected repeating block in line %d\n", lineNumber);
                buf.PrintBytes();
            }

            ++lineNumber;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set 2: Block crypto
///////////////////////////////////////////////////////////////////////////////////////////////////
namespace set2
{
    using MCRYPTO::IEncryptingOracle;

    void challenge9()
    {
        using MCRYPTO::AddPKCS7Padding;

        printf("\n===== Challenge 9 =====\n\n");

        auto padded = AddPKCS7Padding("YELLOW SUBMARINE", 20);

        padded.PrintString();
        padded.PrintBytes();
    }

    void challenge10()
    {
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::EncryptAES128CBC;
        using MCRYPTO::DecryptAES128CBC;

        printf("\n===== Challenge 10 =====\n\n");

        ByteBufferT key( "YELLOW SUBMARINE" );
        ByteBufferT iv( key.Length() );
        iv.Clear();

        auto rawInput = ByteBufferT::ReadFromFile("Data/10.txt");

        auto enc1 = DecodeBase64(rawInput);
        auto dec1 = DecryptAES128CBC(enc1, key, iv);
        auto enc2 = EncryptAES128CBC(dec1, key, iv);
        auto dec2 = DecryptAES128CBC(enc2, key, iv);

        dec2.PrintString();
    }

    void challenge11()
    {
        using MCRYPTO::DetectRepeatingBlocks;
        using MCRYPTO::AES128EncryptionOracle;

        printf("\n===== Challenge 11 =====\n\n");

        ByteBufferT key(  "YELLOW SUBMARINE" );
        ByteBufferT text( "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef");

        AES128EncryptionOracle oracle;
        oracle.GeneratePrefix(5,10);
        oracle.GenerateSuffix(5,10);

        for (auto i=1; i<=10; i++)
        {
            printf("Test %d\n", i);

            auto encrypted = oracle.Encrypt(text);
            bool detectedECB = DetectRepeatingBlocks(encrypted, 16, true, true).Found;

            if (detectedECB && oracle.WasUsingECB())
                printf(" -> Detected ECB.\n");
            else if (!detectedECB && !oracle.WasUsingECB())
                printf(" -> Detected CBC.\n");
            else
                assert(false);
        }
    }

    class C12Oracle : public IEncryptingOracle
    {
        using OracleT = MCRYPTO::AES128EncryptionOracle;

    public:
        explicit C12Oracle(bool withPrefix)
        {
            using MCRYPTO::DecodeBase64;

            oracle_.Mode = OracleT::mode_ecb;
            if (withPrefix)
                oracle_.GeneratePrefix(0,128);

            std::string mysteryStr;
            mysteryStr += "Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkg";
            mysteryStr += "aGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBq";
            mysteryStr += "dXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUg";
            mysteryStr += "YnkK";

            mystery_ = DecodeBase64(mysteryStr.c_str());
        }

        ByteBufferT Encrypt(const ByteBufferT& input) override
        {
            return oracle_.Encrypt(Concat(input, mystery_));
        }

        const ByteBufferT& Mystery() const  { return mystery_; }


    private:
        OracleT     oracle_;
        ByteBufferT mystery_;
    };

    void challenge12()
    {
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::Concat;
        using MCRYPTO::DetectRepeatingBlocks;
        using MCRYPTO::FindKeySize;
        using MCRYPTO::AES128EncryptionOracle;
        using MCRYPTO::ExecuteECB1ByteAttack;

        printf("\n===== Challenge 12 =====\n\n");

        // Setup oracle
        C12Oracle oracle( false );

        // Get keysize
        size_t keySize = FindKeySize(oracle, 64);
        assert(keySize == 16);

        // No other input besides appended mystery string
        size_t mysteryLen = oracle.Encrypt(ByteBufferT( )).Length();
        assert(mysteryLen == oracle.Mystery().Length());

        // Confirm its ECB
        ByteBufferT ecbTest( "0123456789abcdef0123456789abcdef0123456789");
        assert(DetectRepeatingBlocks(oracle.Encrypt(ecbTest), 16, true, true).Found == true);

        auto secret = ExecuteECB1ByteAttack(oracle, keySize);
        assert(secret == oracle.Mystery());

        secret.PrintString();
    }

    void challenge13()
    {
        using MCRYPTO::ParseCookieString;
        using MCRYPTO::EncodeCookie;
        using MCRYPTO::GenerateProfileCookie;
        using MCRYPTO::GenerateRandomBytes;
        using MCRYPTO::EncryptAES128ECB;
        using MCRYPTO::DecryptAES128ECB;
        using MCRYPTO::AddPKCS7Padding;
        

        printf("\n===== Challenge 13 =====\n\n");

        // Get a 16 byte block "admin . padding"
        // 1: email=AAAAAAAAAA
        // 2: adminPPPPPPPPPPP
        // 3: @gmx.de&...
        // ...

        auto key = GenerateRandomBytes(16);
        
        std::string adminStr("AAAAAAAAAA");
        adminStr.append("admin");
        adminStr.append(11, '\x11'); // padding
        adminStr.append("@gmx.de");

        auto cookie1    = GenerateProfileCookie(adminStr.c_str());
        auto encoded1   = AddPKCS7Padding(EncodeCookie(cookie1), 16);
        auto encrypted1 = EncryptAES128ECB(encoded1, key);

        // Replace the "user . padding" block with the previously created admin block
        // 1: email=h4xx0r@gmx
        // 2: .de&uid=10&role=
        // 3: userXXXXXXXXXXXX

        auto cookie2    = GenerateProfileCookie("h4xx0r@gmx.de");
        auto encoded2   = AddPKCS7Padding(EncodeCookie(cookie2), 16);
        auto encrypted2 = EncryptAES128ECB(encoded2, key);

        std::copy(
            encrypted1.begin() + 16,
            encrypted1.begin() + 32,
            encrypted2.begin() + 32);

        auto decrypted = DecryptAES128ECB(encrypted2, key);
        decrypted.Truncate(11);

        decrypted.PrintString();
    }

    void challenge14()
    {

        using MCRYPTO::AES128EncryptionOracle;
        using MCRYPTO::ExecuteECB1ByteAttack;

        printf("\n===== Challenge 14 =====\n\n");

        // Create oracle with random prefix
        C12Oracle oracle( true );

        // Get block-/keysize
        size_t blockSize = FindKeySize2(oracle, 64);
        assert(blockSize == 16);

        // Confirm its ECB
        ByteBufferT ecbTest( "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef");
        assert(DetectRepeatingBlocks(oracle.Encrypt(ecbTest), 16, true, true).Found == true);

        auto secret = ExecuteECB1ByteAttack(oracle, 16);

        secret.PrintString();
    }

    void challenge15()
    {
        using MCRYPTO::ValidatePKCS7Padding;

        printf("\n===== Challenge 15 =====\n\n");

        assert(ValidatePKCS7Padding("ICE ICE BABY\x04\x04\x04\x04") == true);
        assert(ValidatePKCS7Padding("ICE ICE BABY\x05\x05\x05\x05") == false);
        assert(ValidatePKCS7Padding("ICE ICE BABY\x01\x02\x03\x04") == false);

        printf("All good.\n");
    }

    class C16Oracle
    {
    public:
        C16Oracle() :
            key_( MCRYPTO::GenerateRandomBytes(16) ),
            iv_( MCRYPTO::GenerateRandomBytes(16) )
        {}

        ByteBufferT Encrypt(const ByteBufferT& input)
        {
            using MCRYPTO::AddPKCS7Padding;
            using MCRYPTO::EncryptAES128CBC;
            using MCRYPTO::Concat;
            using MCRYPTO::ReplaceChar;

            using DictT = std::vector<std::pair<char, std::string>>;

            auto safeInput = ReplaceChar<ByteBufferT>(input,
                DictT
                {
                    std::make_pair(';', "&#59;"),
                    std::make_pair('=', "&#61;")
                });

            auto data = Concat(
                Concat("comment1=cooking%20MCs;userdata=", safeInput),
                ";comment2=%20like%20a%20pound%20of%20bacon");

            return EncryptAES128CBC(AddPKCS7Padding(data, 16), key_, iv_);
        }

        bool DecryptAndCheckForAdmin(const ByteBufferT& input)
        {
            using MCRYPTO::DecryptAES128CBC;
            using MCRYPTO::ParseCookieString;

            auto decrypted = DecryptAES128CBC(input, key_, iv_);

            decrypted.PrintString();

            // Todo: Too convoluted
            std::string s( decrypted.begin(), decrypted.end() );
            auto cookie = ParseCookieString(s.c_str(), ';');

            return cookie["admin"] == "true";
        }

    private:
        ByteBufferT key_;
        ByteBufferT iv_;
    };

    void challenge16()
    {
        printf("\n===== Challenge 16 =====\n\n");

        C16Oracle helper;

        auto encrypted = helper.Encrypt("bAAAAAcAAAAAAAAAxadminytrue");

        printf("Before change:\n");
        helper.DecryptAndCheckForAdmin(encrypted);
        if (helper.DecryptAndCheckForAdmin(encrypted))
            printf("Admin rights granted!\n");

        encrypted[32] ^= 'x' ^ ';';
        encrypted[38] ^= 'y' ^ '=';

        printf("\nAfter change:\n");
        if (helper.DecryptAndCheckForAdmin(encrypted))
            printf("Admin rights granted!\n");
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set 3: Block & stream crypto
///////////////////////////////////////////////////////////////////////////////////////////////////
namespace set3
{
    using MCRYPTO::IPaddingOracle;

    class C17Oracle : public IPaddingOracle
    {
    public:
        C17Oracle() :
            key_( MCRYPTO::GenerateRandomBytes(16) ),
            iv_( MCRYPTO::GenerateRandomBytes(16) )
        {
            using MCRYPTO::DecodeBase64;
            using MCRYPTO::GetRandomGeneratorInstance;
            using MCRYPTO::GenerateRandomBytes;
            using MCRYPTO::AddPKCS7Padding;
            using MCRYPTO::ValidatePKCS7Padding;
            using MCRYPTO::EncryptAES128CBC;
            using MCRYPTO::DecryptAES128CBC;

            const char* strings[] =
            {
                "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
                "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
                "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
                "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
                "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
                "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
                "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
                "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
                "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
                "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"
            };

            auto& gen = GetRandomGeneratorInstance();
            std::uniform_int_distribution<size_t> dist
            (
                0,
                (sizeof(strings) / sizeof(const char*)) - 1
            );
            size_t index = dist(gen);

            plaintext_  = DecodeBase64(strings[index]);
            ciphertext_ = EncryptAES128CBC(
                AddPKCS7Padding(plaintext_, 16),
                key_, iv_);
        }

        const ByteBufferT& Ciphertext() const   { return ciphertext_; }
        const ByteBufferT& Plaintext() const    { return plaintext_; }
        const ByteBufferT& IV() const           { return iv_; }

        virtual bool DecryptAndValidatePadding(const ByteBufferT& inBuf) override
        {
            return ValidatePKCS7Padding(DecryptAES128CBC(inBuf, key_, iv_));
        }

    private:
        ByteBufferT plaintext_;
        ByteBufferT ciphertext_;
        ByteBufferT key_;
        ByteBufferT iv_;
    };

    void challenge17()
    {
        using MCRYPTO::RemovePKCS7Padding;

        printf("\n===== Challenge 17 =====\n\n");

        C17Oracle oracle;

        // IV || ciphertext
        const auto data = Concat(oracle.IV(), oracle.Ciphertext());

        const size_t blockLen = 16;
        const size_t blockCount = data.Length() / blockLen;
        assert(data.Length() % blockLen == 0);
        
        // First block in test buffer is the one we change.
        // Second block is what we want to decrypt.
        // This buffer is passed to the oracle.
        ByteBufferT testBuf( 2 * blockLen );
        
        // Intermediate state of current block
        ByteBufferT imstBuf( blockLen );
        
        // The final decrypted data
        ByteBufferT resultBuf( (blockCount-1) * blockLen );

        // Skip the first block of data as it's the IV
        // Marks the range that is currently decrypted
        auto blockStart   = data.begin() + blockLen;
        auto blockEnd     = blockStart   + blockLen;

        auto xorPos    = data.begin();
        auto resultPos = resultBuf.begin();

        for (size_t i=1; i<blockCount; i++)
        {
            // Start at last position of current decrypt block and work backwards
            auto testPos    = testBuf.begin() + (blockLen-1);

            testBuf.Clear('=');
            imstBuf.Clear();
            
            std::copy(blockStart, blockEnd, testBuf.begin() + blockLen);

            for (size_t j=0; j<blockLen; j++)
            {
                ByteT   padLen      = j + 1;
                size_t  byteIndex   = blockLen - j - 1;

                for (size_t k=byteIndex+1; k<blockLen; k++)
                    testBuf[k] = imstBuf[k] ^ padLen;

                bool foundMatch = false;

                for (size_t c=0; c<256; c++)
                {
                    *testPos = c;

                    if (oracle.DecryptAndValidatePadding(testBuf))
                    {
                        ByteT b = c;
                        ByteT r = b ^ padLen;

                        imstBuf[byteIndex] = r;

                        foundMatch = true;
                        break;
                    }
                }

                assert(foundMatch);

                --testPos;
            }

            std::transform(
                imstBuf.begin(), imstBuf.end(), xorPos, resultPos,
                std::bit_xor<ByteT>());

            resultPos  += blockLen;
            xorPos     += blockLen;

            blockStart += blockLen;
            blockEnd   += blockLen;
        }

        printf("Original:\n");
        oracle.Plaintext().PrintString();

        printf("Decrypted:\n");
        RemovePKCS7Padding(resultBuf).PrintString();
    }

    void challenge18()
    {
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::AES128CTREngine;

        printf("\n===== Challenge 18 =====\n\n");

        const char* str = "L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ==";

        auto decoded = DecodeBase64(str);

        AES128CTREngine ctrStream( ByteBufferT( "YELLOW SUBMARINE" ), 0 );

        auto result = ctrStream(decoded);

        result.PrintString();
    }

    void challenge19()
    {
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::AES128CTREngine;

        printf("\n===== Challenge 19 =====\n\n");

        // I think I'll skip this one ;P

        /*std::vector<ByteBufferT> plaintexts
        {
	        DecodeBase64("SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ=="),
	        DecodeBase64("Q29taW5nIHdpdGggdml2aWQgZmFjZXM="),
	        DecodeBase64("RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ=="),
	        DecodeBase64("RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4="),
	        DecodeBase64("SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk"),
	        DecodeBase64("T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA=="),
	        DecodeBase64("T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ="),
	        DecodeBase64("UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA=="),
	        DecodeBase64("QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU="),
	        DecodeBase64("T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl"),
	        DecodeBase64("VG8gcGxlYXNlIGEgY29tcGFuaW9u"),
	        DecodeBase64("QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA=="),
	        DecodeBase64("QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk="),
	        DecodeBase64("QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg=="),
	        DecodeBase64("QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo="),
	        DecodeBase64("QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4="),
	        DecodeBase64("VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA=="),
	        DecodeBase64("SW4gaWdub3JhbnQgZ29vZCB3aWxsLA=="),
	        DecodeBase64("SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA=="),
	        DecodeBase64("VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg=="),
	        DecodeBase64("V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw=="),
	        DecodeBase64("V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA=="),
	        DecodeBase64("U2hlIHJvZGUgdG8gaGFycmllcnM/"),
	        DecodeBase64("VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w="),
	        DecodeBase64("QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4="),
	        DecodeBase64("VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ="),
	        DecodeBase64("V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs="),
	        DecodeBase64("SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA=="),
	        DecodeBase64("U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA=="),
	        DecodeBase64("U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4="),
	        DecodeBase64("VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA=="),
	        DecodeBase64("QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu"),
	        DecodeBase64("SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc="),
	        DecodeBase64("VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs"),
	        DecodeBase64("WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs="),
	        DecodeBase64("SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0"),
	        DecodeBase64("SW4gdGhlIGNhc3VhbCBjb21lZHk7"),
	        DecodeBase64("SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw="),
	        DecodeBase64("VHJhbnNmb3JtZWQgdXR0ZXJseTo="),
	        DecodeBase64("QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=")
        };*/
    }

    void challenge20()
    {
        using MCRYPTO::AES128CTREngine;
        using MCRYPTO::DecodeBase64;
        using MCRYPTO::GenerateRandomBytes;
        using MCRYPTO::FindXorKey;
        using MCRYPTO::Xor;

        printf("\n===== Challenge 20 =====\n\n");

        AES128CTREngine keyStream(GenerateRandomBytes(16), 0);

        std::ifstream inFile("Data/20.txt");

        assert(inFile.is_open());

        std::string line;

        std::vector<ByteBufferT> ciphertexts;

        size_t minLen = (std::numeric_limits<size_t>::max)();

        while (std::getline(inFile, line))
        {
            auto decoded   = DecodeBase64(line.c_str());
            auto encrypted = keyStream(decoded);

            size_t len = encrypted.Length();
            if (minLen > len)
                minLen = len;

            ciphertexts.push_back(std::move(encrypted));

            keyStream.Reset();
        }

        ByteBufferT merged( minLen * ciphertexts.size() );

        auto mergedOut = merged.begin();

        for (const auto& v : ciphertexts)
        {
            std::copy(v.begin(), v.begin() + minLen, mergedOut);
            mergedOut += minLen;
        }

        ByteBufferT keyBuf( minLen );
        auto keyOut = keyBuf.begin();

        Transpose(merged, minLen,
            [&] (const ByteBufferT& block)
            {
                *keyOut = FindXorKey(block).Key;
                ++keyOut;
            });

        for (const auto& v : ciphertexts)
        {
            auto decrypted = Xor(keyBuf, v);
            decrypted.TruncateTo(minLen);
            decrypted.PrintString();
        }
    }

    void challenge21()
    {
        using MCRYPTO::MT19937;

        printf("\n===== Challenge 21 =====\n\n");

        MT19937 randgen1( 1324 );

        std::mt19937 randgen2( 1324 );

        for (size_t i=0; i<1024*1024; i++)
            assert(randgen1() == randgen2());

        printf("All good.\n");
    }

    void challenge22()
    {
        using MCRYPTO::MT19937;

        printf("\n===== Challenge 22 =====\n\n");

        std::this_thread::sleep_for( std::chrono::seconds( 10 ) );

        uint32_t targetVal;
        
        {
            std::time_t t = std::time(0);
            uint32_t seed = static_cast<uint32_t>(t);
            MT19937 gen( seed );

            targetVal = gen();
        }
        
        std::this_thread::sleep_for( std::chrono::seconds( 10 ) );
        
        uint32_t curTime = static_cast<uint32_t>(std::time(0));

        printf("Starting search...\n");

        for (uint32_t seed = curTime - 0x00278D00; seed < curTime; seed++)
        {
            MT19937 gen( seed );

            if (gen() == targetVal)
                printf("Seed is %d\n", seed);
        }
    }

    void challenge23()
    {
        using MCRYPTO::MT19937;
        using MCRYPTO::MT19937Untemper;

        static const uint32_t n = 624;
        uint32_t state[n];

        printf("\n===== Challenge 23 =====\n\n");

        MT19937 clonee( 12345 );

        for (size_t i=0; i<n; i++)
            state[i] = MT19937Untemper(clonee());

        MT19937 cloned( state );

        for (size_t i=0; i<10; i++)
            assert(cloned() == clonee());

        printf("Cloned.\n");
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Main
///////////////////////////////////////////////////////////////////////////////////////////////////
void main()
{
    {
        using namespace set1;

        //challenge1();
        //challenge2();
        //challenge3();
        //challenge4();
        //challenge5();
        //challenge6();
        //challenge7();
        //challenge8();
    }

    {
        using namespace set2;

        //challenge9();
        //challenge10();
        //challenge11();
        //challenge12();
        //challenge13();
        //challenge14();
        //challenge15();
        //challenge16();
    }

    {
        using namespace set3;

        //challenge17();
        //challenge18();
        //challenge19();
        //challenge20();
        //challenge21();
        //challenge22();
        challenge23();
    }
}