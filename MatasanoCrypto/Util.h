#pragma once

#include "Defs.h"

#include <utility>
#include <random>
#include <unordered_map>

#include "Containers.h"

/****************************************/ MCRYPTO_BEGIN /****************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////
/// GetValueType
///////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
struct GetValueType
{
    using Type = typename T::value_type;
};

template <typename T, size_t n>
struct GetValueType<T[n]>
{
    using Type = T;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
/// GetRangeLength
///////////////////////////////////////////////////////////////////////////////////////////////////
template <typename T>
size_t GetRangeLength(const T& range)
{
    return std::distance(std::begin(range), std::end(range));
}

template <typename T>
size_t GetRangeLength(const FixedBuffer<T>& buf)
{
    return buf.Length();
}

template <typename T>
size_t GetRangeLength(const char* str)
{
    return strlen(buf);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Hex strings
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteT       DecodeHexCharacter(char c);

ByteBufferT DecodeHexString(const char* inStr);
void        DecodeHexString(const char* inStr, ByteBufferT& outBuf);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Base64 strings
///////////////////////////////////////////////////////////////////////////////////////////////////
char        EncodeBase64Character(ByteT c);

ByteT       DecodeBase64Character(char c);

ByteBufferT EncodeBase64(const ByteBufferT& inBuf);

ByteBufferT DecodeBase64(const ByteBufferT& inBuf);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Byte buffer utils
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT Concat(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// XOR on byte buffers
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT Xor(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2);
void        Xor(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2, ByteBufferT& outBuf);
void        Xor(const ByteT* in1, const ByteT* in2, ByteT* out, size_t length);

ByteBufferT Xor(const ByteBufferT& inBuf, const ByteT inChar);
void        Xor(const ByteBufferT& inBuf, const ByteT inChar, ByteBufferT& outBuf);

ByteBufferT RepeatingXor(const ByteBufferT& inBuf, const ByteBufferT& keySeq);
void        RepeatingXor(const ByteBufferT& inBuf, const ByteBufferT& keySeq, ByteBufferT& outBuf);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Statistical string analysis
///////////////////////////////////////////////////////////////////////////////////////////////////
int         CalculateCharacterScore(const ByteBufferT& inBuf);

struct XorKeyResult
{
    ByteT   Key;
    int     Score;
};

XorKeyResult FindXorKey(const ByteBufferT& inBuf);
XorKeyResult FindXorKey(const ByteBufferT& inBuf, ByteBufferT& xorBuf);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Hamming distance
///////////////////////////////////////////////////////////////////////////////////////////////////
int CalculateHammingDistance(const ByteBufferT& inBuf1, const ByteBufferT& inBuf2);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ECB/CBC detection
///////////////////////////////////////////////////////////////////////////////////////////////////
struct RepeatingBlocksResult
{
    bool    Found;
    size_t  Offset;
};

RepeatingBlocksResult DetectRepeatingBlocks(const ByteBufferT& inBuf, size_t blockSize,
                                            bool aligned, bool successive);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Padding
///////////////////////////////////////////////////////////////////////////////////////////////////
ByteBufferT AddPKCS7Padding(const ByteBufferT& inBuf, size_t blockSize);
ByteBufferT RemovePKCS7Padding(const ByteBufferT& inBuf);

bool        ValidatePKCS7Padding(const ByteBufferT& inBuf);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Randomness
///////////////////////////////////////////////////////////////////////////////////////////////////
std::mt19937& GetRandomGeneratorInstance();

ByteBufferT GenerateRandomBytes(size_t length);
void        GenerateRandomBytes(ByteBufferT& outBuf);
void        GenerateRandomBytes(ByteT* start, ByteT* end);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Oracle interfaces
///////////////////////////////////////////////////////////////////////////////////////////////////
struct IEncryptingOracle
{
    virtual ByteBufferT Encrypt(const ByteBufferT& inBuf) = 0;
};

struct IDecryptingOracle
{
    virtual ByteBufferT Decrypt(const ByteBufferT& inBuf) = 0;
};

struct IPaddingOracle
{
    virtual bool DecryptAndValidatePadding(const ByteBufferT& inBuf) = 0;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
/// AES128 ECB/CBC oracle
///////////////////////////////////////////////////////////////////////////////////////////////////
class AES128EncryptionOracle : public IEncryptingOracle
{
public:
    enum EMode { mode_random, mode_ecb, mode_cbc };

    AES128EncryptionOracle();

    void GenerateNewKey();
    void GenerateNewIV();
    void GeneratePrefix(size_t min, size_t max = 0);
    void GenerateSuffix(size_t min, size_t max = 0);

    ByteBufferT Encrypt(const ByteBufferT& inBuf) override;

    inline bool WasUsingECB() const    { return shouldUseECB_; }

    inline size_t PrefixLength() const  { return prefix_.Length(); }
    inline size_t SuffixLength() const  { return suffix_.Length(); }

    EMode   Mode;

private:
    ByteBufferT key_;
    ByteBufferT iv_;
    ByteBufferT prefix_;
    ByteBufferT suffix_;

    std::uniform_int_distribution<int>      coinFlipDist_;

    bool shouldUseECB_;
};

///////////////////////////////////////////////////////////////////////////////////////////////////
/// ECB attacks
///////////////////////////////////////////////////////////////////////////////////////////////////
size_t FindKeySize(IEncryptingOracle& oracle, size_t maxSize);
size_t FindKeySize2(IEncryptingOracle& oracle, size_t maxSize);

ByteBufferT ExecuteECB1ByteAttack(IEncryptingOracle& oracle, size_t keySize);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Cookie parser
///////////////////////////////////////////////////////////////////////////////////////////////////
using CookieT = std::unordered_map<std::string, std::string>;

CookieT     ParseCookieString(const char* str, char separator = '&');

ByteBufferT EncodeCookie(const CookieT& cookie);

CookieT     GenerateProfileCookie(const char* email);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Mersenne twister
///////////////////////////////////////////////////////////////////////////////////////////////////
class MT19937
{
public:
    static const uint32_t n = 624;
    static const uint32_t m = 397;

    explicit MT19937(uint32_t seed);
    explicit MT19937(uint32_t* initialState);

    uint32_t operator()();

private:
    void initialize(uint32_t seed);
    void generateNext();

    uint32_t index_;
    uint32_t y_[n];
};

uint32_t MT19937Untemper(uint32_t value);

/*****************************************/ MCRYPTO_END /*****************************************/